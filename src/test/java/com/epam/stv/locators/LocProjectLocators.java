package com.epam.stv.locators;

import org.openqa.selenium.By;

/**
 * Created by Tatiana_Sauchanka on 2/19/2017.
 */

public class LocProjectLocators {
    public static final String START_URL = "https://www.wiggle.com/secure/myaccount/logon?returnurl=%2F&forceLogIn=True";
    public static final String WARNING_TEXT = "Sorry we could not log you in.";
    public static final String USER_EMAIL = "newUser125@stv.com";
    public static final String USER_PASSWORD = "STVproject";
    public static final String REGISTER_URL = "https://www.wiggle.com/secure/myaccount/logon?returnurl=%2F&forceRegister=True";
    public static final String LOGIN_URL = "https://www.wiggle.com/secure/myaccount/logon?returnurl=%2F&forceLogIn=True";
    public static final String SAUCELOGIN = "dzowl";
    public static final String ACCESS_KEY = "82cc9378-f5e7-4d35-8c02-cc3a8a3f4d14";

    //---------------------------locators----------------------------------------------------------------------------------
    public static final By WIGGLE_ICON_LOCATOR = By.cssSelector(".bem-checkout__logo");



}
