package com.epam.stv.html_elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.pagefactory.CustomElementLocatorFactory;

/**
 * Created by Tatiana_Sauchanka on 4/9/2017.
 */
public class WiggleMainPage {
    private WebDriver driver;

    WiggleMainPage(final WebDriver driver){
        PageFactory.initElements(new HtmlElementDecorator((CustomElementLocatorFactory) driver), this);
        this.driver = driver;
    }

}
