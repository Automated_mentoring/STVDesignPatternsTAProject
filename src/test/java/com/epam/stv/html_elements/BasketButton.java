package com.epam.stv.html_elements;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;


/**
 * Created by Tatiana_Sauchanka on 4/9/2017.
 */
public class BasketButton extends HtmlElement {
    @FindBy(xpath = "//a[@class='bem-mini-basket__link--empty']")
    private Button basketbutton;

    public void clickBasketButton() {
        basketbutton.click();
    }

}
