package com.epam.stv.factory.factorypages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tatiana_Sauchanka on 3/7/2017.
 */
public abstract class FactoryPage {

    protected  WebDriver driver;

    public FactoryPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }


    public String assertCurrentURL(){
        String currentUrl = driver.getCurrentUrl();
        return currentUrl;
    }

}
